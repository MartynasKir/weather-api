## Description
This is weather app back-end api. Application uses PostgreSQL database to persist hourly Vilnius temperature data
 history of specified amount of days: configured by `history` parameter in `application.yml`. Currently set to 28 days.
* History data synchronization is made on startup.
    * Check if each `history` day of "temperatures" table consist enough data records: Configurable `dailyrecords` in
     `application.yml` and if there is not enough data, table is updated for that day only. 
    * If there is enough data for particular day, synchronization process skip that day. 
* During runtime app automatically update "temperatures" table hourly for the current day.

## Running application
1. Clone project to local repository
2. Open with IDE (Intellij, Eclipse)
3. Set `apikey` value in `application.yml` to working ClimaCell apikey (for Integration/E2E tests also)
4. Setup local PostgresSQL instance and update parameters under `datasource` in `application.yml`
5. Run project (WeatherApiApplication class)
6. Using web client (browser) go to http://localhost:8080/weather/api/v1/temperature/history to return temperature history in Vilnius city
7. All temperature data is fetched from configured database and returned to web client (browser)

## Integration tests
For integration tests only, a working `apikey` is required.
Tests use in memory H2 database.
`currently 100% clases & 79% lines covered by integration tests`

## Request& Response
#### Expected request:
GET method to http://localhost:8080/weather/api/v1/temperature/history

#### Expected Response:
```
[
{
        "id": 194,
        "temperatureString": "16C",
        "city": "vilnius",
        "observationTime": "2020-07-02T23:50:00.000Z"
    },
    {
        "id": 193,
        "temperatureString": "16C",
        "city": "vilnius",
        "observationTime": "2020-07-02T23:20:00.000Z"
    },
    {
        "id": 192,
        "temperatureString": "17C",
        "city": "vilnius",
        "observationTime": "2020-07-02T22:50:00.000Z"
    },
    {
        "id": 191,
        "temperatureString": "18C",
        "city": "vilnius",
        "observationTime": "2020-07-02T22:20:00.000Z"
    }
]
```
