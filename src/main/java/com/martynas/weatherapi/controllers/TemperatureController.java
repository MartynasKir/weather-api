package com.martynas.weatherapi.controllers;

import com.martynas.weatherapi.entities.TemperatureRecord;
import com.martynas.weatherapi.services.TemperatureService;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/weather/api/v1/temperature")
public class TemperatureController {

    private final TemperatureService temperatureService;

    public TemperatureController(TemperatureService temperatureService) {
        this.temperatureService = temperatureService;
    }

    @GetMapping(value = "history")
    public List<TemperatureRecord> getAllTemp() {
        return temperatureService.findAllTemp();
    }
}
