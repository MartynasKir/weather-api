package com.martynas.weatherapi.repositories;

import com.martynas.weatherapi.entities.TemperatureRecord;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemperatureRepository extends JpaRepository<TemperatureRecord, Long> {

    List<TemperatureRecord> findByObservationTimeLike(String date);

    Integer deleteByObservationTimeLike(String date);

}
