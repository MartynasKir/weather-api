package com.martynas.weatherapi.configs;

import java.util.HashMap;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "app.cities")
@ConstructorBinding
public class LocationProperties {

    private HashMap<String, String> vilnius;

    public LocationProperties(HashMap<String, String> vilnius) {
        this.vilnius = vilnius;
    }

    public HashMap<String, String> getCoordinates() {
        return vilnius;
    }
}
