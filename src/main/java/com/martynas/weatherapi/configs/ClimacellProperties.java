package com.martynas.weatherapi.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "app.climacell")
@ConstructorBinding
public class ClimacellProperties {

    private final String hostname;
    private final String apikey;

    public ClimacellProperties(String hostname, String apikey) {
        this.hostname = hostname;
        this.apikey = apikey;
    }

    public String getHostname() {
        return hostname;
    }

    public String getApikey() {
        return apikey;
    }
}
