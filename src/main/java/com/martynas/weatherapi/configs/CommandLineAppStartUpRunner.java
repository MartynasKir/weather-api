package com.martynas.weatherapi.configs;

import com.martynas.weatherapi.services.TemperatureService;
import java.time.LocalDate;
import java.time.ZoneOffset;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CommandLineAppStartUpRunner implements CommandLineRunner {

    private static final Logger LOG = LoggerFactory.getLogger(CommandLineAppStartUpRunner.class);

    @Autowired
    private TemperatureService temperatureService;

    @Autowired
    DataSyncProperties dataSyncProperties;

    @Override
    public void run(String... args) throws Exception {
        LOG.info("Data sunchronization initiated...");
        LocalDate startDate = LocalDate.now(ZoneOffset.UTC);
        LocalDate endDate = startDate.plusDays(1);

        int dataHistoryDays = dataSyncProperties.getHistory();
        while (dataHistoryDays > 0) {
            LOG.info("Synchronization check for period: {} - {}", startDate, endDate);
            String stringStartDate = startDate.toString();
            String stringEndDate = endDate.toString();

            int recordListSize = temperatureService.findByObservationTimeLike(stringStartDate).size();
            //check if table has enough record for given day
            if (recordListSize < dataSyncProperties.getDailyRecords()) {
                LOG.info("Not enough records for period : {} - {}, initiate data reload...",
                        startDate, endDate);
                temperatureService.updateRecords(stringStartDate, stringEndDate);
            }
            LOG.info("OK. Sufficient data exists for period: {} - {}", startDate, endDate);
            startDate = startDate.minusDays(1);
            endDate = endDate.minusDays(1);
            dataHistoryDays--;
        }
        LOG.info("Data synchronization finished");
    }
}
