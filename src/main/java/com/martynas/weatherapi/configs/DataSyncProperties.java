package com.martynas.weatherapi.configs;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConfigurationProperties(prefix = "app.datasync")
@ConstructorBinding
public class DataSyncProperties {

    private final int history;
    private final int dailyrecords;

    public DataSyncProperties(int history, int dailyrecords) {
        this.history = history;
        this.dailyrecords = dailyrecords;
    }

    public int getHistory() {
        return history;
    }

    public int getDailyRecords() {
        return dailyrecords;
    }

}
