package com.martynas.weatherapi.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "temperatures")
public class TemperatureRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @Column(name = "temperature", nullable = false)
    private String temperatureString; //contain string of temp + units: "18C"

    @Column(name = "city", nullable = false)
    private String city;

    @Column(name = "observation_time", nullable = false)
    private String observationTime;

    public TemperatureRecord() {
        //Default constructor for entity creation
    }

    public TemperatureRecord(Long id, String city, String temperatureString, String observationTime) {
        this.id = id;
        this.city = city;
        this.temperatureString = temperatureString;
        this.observationTime = observationTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemperatureString() {
        return temperatureString;
    }

    public void setTemperatureString(String temperatureString) {
        this.temperatureString = temperatureString;
    }

    public String getObservationTime() {
        return observationTime;
    }

    public void setObservationTime(String observationTime) {
        this.observationTime = observationTime;
    }
}
