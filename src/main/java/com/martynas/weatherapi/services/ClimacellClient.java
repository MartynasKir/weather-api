package com.martynas.weatherapi.services;

import com.martynas.weatherapi.configs.ClimacellProperties;
import com.martynas.weatherapi.configs.LocationProperties;
import com.martynas.weatherapi.dtos.TemperatureDto;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ClimacellClient {

    private static final Logger LOG = LoggerFactory.getLogger(ClimacellClient.class);

    private static final String WEATHER_HISTORY_ENDPOINT = "v3/weather/historical/station";

    private final LocationProperties locationProperties;
    private final RestTemplate restTemplate;
    private final HttpHeaders headers;
    private final String hostname;

    public ClimacellClient(ClimacellProperties climacellProperties,
            LocationProperties locationProperties,
            RestTemplate restTemplate) {
        this.locationProperties = locationProperties;
        this.restTemplate = restTemplate;
        this.headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("apikey", climacellProperties.getApikey());

        hostname = climacellProperties.getHostname();
    }

    public List<TemperatureDto> getTemperatureForPeriod(String start_time, String end_time) {

        String URI = hostname + WEATHER_HISTORY_ENDPOINT;
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URI)
                .queryParam("lat", locationProperties.getCoordinates().get("lat"))
                .queryParam("lon", locationProperties.getCoordinates().get("lon"))
                .queryParam("start_time", start_time)
                .queryParam("end_time", end_time)
                .queryParam("fields", List.of("temp"));
        LOG.info("Send request to ClimaCell api: {}", builder.toUriString());

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List<TemperatureDto>> response = restTemplate
                .exchange(builder.toUriString(), HttpMethod.GET, entity,
                        new ParameterizedTypeReference<List<TemperatureDto>>() {
                        });
        return response.getBody();
    }
}
