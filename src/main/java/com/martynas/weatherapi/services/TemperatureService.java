package com.martynas.weatherapi.services;

import com.martynas.weatherapi.dtos.TemperatureDto;
import com.martynas.weatherapi.entities.TemperatureRecord;
import com.martynas.weatherapi.repositories.TemperatureRepository;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TemperatureService {

    private static final String DATE_ENDING = "T00:30:00Z";
    private static final Logger LOG = LoggerFactory.getLogger(TemperatureService.class);
    private final TemperatureRepository temperatureRepository;
    private final ClimacellClient climacellClient;
    private final ModelMapper modelMapper;

    public TemperatureService(TemperatureRepository temperatureRepository,
            ClimacellClient climacellClient) {
        this.temperatureRepository = temperatureRepository;
        this.climacellClient = climacellClient;
        this.modelMapper = new ModelMapper();
    }


    public List<TemperatureRecord> findAllTemp() {
        return (List<TemperatureRecord>) temperatureRepository.findAll(Sort.by(Direction.DESC, "observationTime"));
    }


    public List<TemperatureRecord> findByObservationTimeLike(String date) {
        String dateLike = date + "%";
        return temperatureRepository.findByObservationTimeLike(dateLike);
    }


    @Transactional
    public void deleteByObservationTimeLike(String date) {
        String dateLike = date + "%";
        Integer deletedRecords = temperatureRepository.deleteByObservationTimeLike(dateLike);
        LOG.info("Deleted {} records for date: {}", deletedRecords, date);
    }


    @Transactional
    public void save(TemperatureDto temperatureDto) {
        temperatureRepository.save(convertToEntity(temperatureDto));
        LOG.debug("Saved record: {}", temperatureDto.toString());
    }


    @Transactional
    public void updateRecords(String startDate, String endDate) {
        deleteByObservationTimeLike(startDate);
        climacellClient.getTemperatureForPeriod(startDate + DATE_ENDING, endDate + DATE_ENDING)
                .stream()
                .forEach(temperatureDto -> save(temperatureDto));
    }


    @Transactional
    @Scheduled(cron = "0 0 * * * *") //scheduled method to run every hour and import new data
    public void scheduledHourlyRecordsUpdate() {
        LOG.info("Hourly data update initiated...");
        LocalDate startDate = LocalDate.now(ZoneOffset.UTC);
        LocalDate endDate = startDate.plusDays(1);

        String stringStartDate = startDate.toString();
        String stringEndDate = endDate.toString();
        LOG.info("Initiate scheduled data update for date: {}", stringStartDate);
        updateRecords(stringStartDate, stringEndDate);
        LOG.info("Scheduled data update completed for date: {}", stringStartDate);
    }


    private TemperatureRecord convertToEntity(TemperatureDto temperatureDto) {
        TemperatureRecord entity = modelMapper.map(temperatureDto, TemperatureRecord.class);
        entity.setCity("vilnius"); //hardcoded temporarily
        entity.setObservationTime(temperatureDto.getObservationTime().getValue());
        entity.setTemperatureString(temperatureDto.getTemp().getValue() + temperatureDto.getTemp().getUnits());
        LOG.debug("Converted DTO to Entity record {}", entity.toString());
        return entity;
    }
}
