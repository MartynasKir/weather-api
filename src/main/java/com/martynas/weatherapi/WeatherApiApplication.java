package com.martynas.weatherapi;

import com.martynas.weatherapi.configs.ClimacellProperties;
import com.martynas.weatherapi.configs.DataSyncProperties;
import com.martynas.weatherapi.configs.LocationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@EnableConfigurationProperties({ClimacellProperties.class, LocationProperties.class, DataSyncProperties.class})
public class WeatherApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(WeatherApiApplication.class, args);
    }

}
