package com.martynas.weatherapi.dtos;

import java.io.Serializable;

public class Temperature implements Serializable {

    private String value;
    private String units;

    public Temperature() {
    }

    public Temperature(String value, String units) {
        this.value = value;
        this.units = units;
    }


    public String getValue() {
        return value;
    }

    public String getUnits() {
        return units;
    }
}
