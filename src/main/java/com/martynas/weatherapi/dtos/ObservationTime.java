package com.martynas.weatherapi.dtos;

import java.io.Serializable;

public class ObservationTime implements Serializable {

    private String value;

    public ObservationTime() {
    }

    public ObservationTime(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
