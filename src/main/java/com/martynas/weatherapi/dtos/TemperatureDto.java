package com.martynas.weatherapi.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;

public class TemperatureDto implements Serializable {

    private final Temperature temp;
    private final ObservationTime observationTime;

    public TemperatureDto(@JsonProperty("temp") Temperature temp,
            @JsonProperty("observation_time") ObservationTime observationTime) {
        this.temp = temp;
        this.observationTime = observationTime;
    }

    public Temperature getTemp() {
        return temp;
    }

    public ObservationTime getObservationTime() {
        return observationTime;
    }
}
