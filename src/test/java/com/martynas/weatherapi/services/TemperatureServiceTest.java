package com.martynas.weatherapi.services;

import com.martynas.weatherapi.dtos.ObservationTime;
import com.martynas.weatherapi.dtos.Temperature;
import com.martynas.weatherapi.dtos.TemperatureDto;
import com.martynas.weatherapi.entities.TemperatureRecord;
import com.martynas.weatherapi.repositories.TemperatureRepository;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

@ExtendWith(MockitoExtension.class)
public class TemperatureServiceTest {

    @Mock
    TemperatureRepository temperatureRepository;

    @InjectMocks
    TemperatureService temperatureService;

    @Test
    public void whenFindAllTemp_thenReturnTemperatureRecords() {

        String expectedTime = "2020-06-30T23:00:00Z";

        //mock
        Mockito.when(temperatureRepository.findAll(Sort.by(Direction.DESC, "observationTime")))
                .thenReturn(List.of(getTemperatureRecord()));

        //execute
        List<TemperatureRecord> temperatures = temperatureService.findAllTemp();

        //validate
        Assertions.assertEquals(expectedTime, temperatures.get(0).getObservationTime());
    }

    private TemperatureRecord getTemperatureRecord() {
        TemperatureRecord temperatureRecord = new TemperatureRecord();
        temperatureRecord.setId(1L);
        temperatureRecord.setTemperatureString("18C");
        temperatureRecord.setCity("vilnius");
        temperatureRecord.setObservationTime("2020-06-30T23:00:00Z");
        return temperatureRecord;
    }

    private TemperatureDto getTemperatureDto() {
        TemperatureDto temperatureDto = new TemperatureDto(
                new Temperature("18", "C"),
                new ObservationTime("2020-06-30T23:00:00Z"));
        return temperatureDto;
    }
}
