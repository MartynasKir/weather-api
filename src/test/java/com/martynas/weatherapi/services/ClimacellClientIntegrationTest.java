package com.martynas.weatherapi.services;

import com.martynas.weatherapi.dtos.TemperatureDto;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ClimacellClientIntegrationTest {

    @Autowired
    ClimacellClient climacellClient;

    @Test
    public void givenCorrentParameters_returnRequiredData() {

        String start_time = "2020-06-29T23:00:00Z";
        String end_time = "2020-06-30T23:00:00Z";

        List<TemperatureDto> temperatures = climacellClient.getTemperatureForPeriod(start_time, end_time);

        Assertions.assertNotNull(temperatures.get(0));
    }
}
